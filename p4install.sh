#!/bin/bash
apt-get update
apt-get install --no-install-recommends ca-certificates
update-ca-certificates
apt-get install --no-install-recommends sudo lsb-release git
apt-get clean
mkdir p4
cd p4
git clone https://github.com/jafingerhut/p4-guide
./p4-guide/bin/install-p4dev-v4.sh
cat p4setup.bash >> ~/.bashrc
cd ~/
rm -rf p4/
