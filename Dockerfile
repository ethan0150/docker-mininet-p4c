FROM ubuntu:focal

USER root
WORKDIR /root
ENV DEBIAN_FRONTEND=noninteractive
ENV TZ=Asia/Taipei
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN echo "APT::Get::Assume-Yes \"true\";" >> /etc/apt/apt.conf.d/90dockerdefault

COPY ENTRYPOINT.sh /
COPY p4install.sh /
RUN sed -i -e 's/http:\/\/archive.ubuntu.com\/ubuntu\//mirror:\/\/mirrors.ubuntu.com\/mirrors.txt/' /etc/apt/sources.list \
 && /p4install.sh
RUN apt-get update && apt-get install --no-install-recommends \
    curl \
    iproute2 \
    iputils-ping \
    mininet \
    net-tools \
    openvswitch-switch \
    openvswitch-testcontroller \
    tcpdump \
    vim \
    neovim \
    x11-xserver-utils \
    xterm \
    python-is-python3 \
 && apt-get remove wireshark \
 && apt-get autoremove \
 && apt-get upgrade \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/* \
 && rm /etc/apt/apt.conf.d/90dockerdefault \
 && chmod +x /ENTRYPOINT.sh

EXPOSE 6633 6653 6640

ENTRYPOINT ["/ENTRYPOINT.sh"]
